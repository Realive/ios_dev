//
//  Card.swift
//  ios_dev
//
//  Created by student on 2020/3/23.
//  Copyright © 2020年 student. All rights reserved.
//

import Foundation

struct Card {
    var flip: Bool = false
    var match: Bool = false
    var codex: Int
    
    init(codex id: Int) {
        self.codex = id
    }
    
    func isFlip() -> Bool {
        return self.flip
    }
    
    func isMatch() -> Bool {
        return self.match
    }
    
    func value() -> Int {
        return self.codex
    }
}


