//
//  Game.swift
//  ios_dev
//
//  Created by student on 2020/3/23.
//  Copyright © 2020年 student. All rights reserved.
//

import Foundation

class Game {
    var cards: Array<Card> = Array<Card>()
    
    func selectCard(index: Int) -> Int {
        if cards[index].isFlip(){
            //cards[index].flip = false
            return 0
        } else {
            cards[index].flip = true
            return cards[index].value()
        }
    }
    
    init(cardSets numbers: Int) {
        for codex in 1...numbers {
            let card = Card(codex: codex)
            self.cards += [card]
            //self.cards.shuffle()
            var shuffled = [Card]()
            for _ in 0..<cards.count
            {
                let rand = Int(arc4random_uniform(UInt32(cards.count)))
                
                shuffled.append(cards[rand])
                
                cards.remove(at: rand)
            }
            cards = shuffled
            //self.cards += [card, card]
        }
    }
    
    func getCards() -> Array<Card> {
        return self.cards
    }
    
    func win(origin:Int, new: Int) -> Bool {
        if origin > new {
            return false
        } else {
            return true
        }
    }
}

