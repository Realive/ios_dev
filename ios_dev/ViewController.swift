//
//  ViewController.swift
//  ios_dev
//
//  Created by student on 2020/3/4.
//  Copyright © 2020年 student. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet var cardCollection: [UIButton]!
    
    @IBOutlet weak var winLabel: UILabel!
    @IBOutlet weak var loseLabel: UILabel!
    //lazy var game = Game(cardSets: cardCollection.count/2)
    lazy var game = Game(cardSets: cardCollection.count)
    
    var originValue: Int = 0
    var newValue:Int = 0
    var flipCount: Int = 0 {
        didSet {
            countLabel.text = "Flips: \(flipCount)"
        }
    }
    
    var winCount: Int = 0 {
        didSet {
            winLabel.text = "Win: \(winCount)"
        }
    }
    
    var loseCount: Int = 0 {
        didSet{
            loseLabel.text = "Lose: \(loseCount)"
        }
    }
    
    func updateView() {
        for index in cardCollection.indices {
            let button = cardCollection[index]
            let card = game.cards[index]
            if card.flip {
                button.setTitle(getEmoji(by: card), for: UIControlState.normal)
                button.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.2)
                //originValue = getLegacyValue(value: card.value())
                //flipCount += 1
            } else {
                //button.setTitle("", for: UIControlState.normal)
            }
        }
    }
    
    func getEmoji(by card: Card) -> String {
        switch (card.value()){
        case 1:
            return "A"
        case 11:
            return "J"
        case 12:
            return "Q"
        case 13:
            return "K"
        default:
            return String(card.value())
        }
    }
    
    func getLegacyValue(value: Int) -> Int {
        if (value == 1) {
            return 14
        } else {
            return value
        }
    }
    
    @IBAction func cardButton(_ sender: UIButton) {
        if let cardNumber = cardCollection.index(of: sender) {
            let val = game.selectCard(index: cardNumber)
            if (val != 0){
                flipCount += 1
                newValue = getLegacyValue(value: val)
                if game.win(origin: originValue, new: newValue) {
                    winCount += 1
                } else {
                    loseCount += 1
                }
                //originValue = newValue
            }
            updateView()
        } else {
            print("not in collection")
        }
    }
    
    func openCard() {
        let rand = arc4random_uniform(UInt32(cardCollection.count))
        originValue = getLegacyValue(value: game.selectCard(index: Int(rand)))
        updateView()
    }
    
//    @IBAction func openButton(_ sender: UIButton) {
//        for index in 0...cardCollection.count-1{
//            game.selectCard(index: index)
//            updateView()
//        }
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openCard()
        //updateView()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

